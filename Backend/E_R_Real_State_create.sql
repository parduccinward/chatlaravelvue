-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-05-22 03:51:37.328

-- tables
-- Table: agent_estate
CREATE TABLE agent_estate (
    id_agent serial  NOT NULL,
    id_company int  NOT NULL,
    car boolean  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    person_id int  NOT NULL,
    CONSTRAINT agent_estate_pk PRIMARY KEY (id_agent)
);

-- Table: assignment
CREATE TABLE assignment (
    id_assignment serial  NOT NULL,
    date timestamp  NOT NULL,
    agent_estate_id_agent int  NOT NULL,
    client_id_client int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT id_assignment PRIMARY KEY (id_assignment)
);

-- Table: buy_review
CREATE TABLE buy_review (
    id_review serial  NOT NULL,
    comment text  NULL,
    stars decimal(10,4)  NOT NULL,
    id_client int  NOT NULL,
    date timestamp  NOT NULL,
    id_property int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT buy_review_pk PRIMARY KEY (id_review)
);

-- Table: chat
CREATE TABLE chat (
    id_chat serial  NOT NULL,
    date timestamp  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    listener_id int  NOT NULL,
    session_id int  NOT NULL,
    CONSTRAINT chat_pk PRIMARY KEY (id_chat)
);

-- Table: client
CREATE TABLE client (
    id_client serial  NOT NULL,
    person_id int  NOT NULL,
    disease boolean  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    id_company int  NOT NULL,
    CONSTRAINT client_pk PRIMARY KEY (id_client)
);

-- Table: company
CREATE TABLE company (
    id_company serial  NOT NULL,
    phone int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT company_pk PRIMARY KEY (id_company)
);

-- Table: images
CREATE TABLE images (
    image_id serial  NOT NULL,
    filename varchar(255)  NOT NULL,
    url_original varchar(255)  NULL,
    url_little varchar(255)  NULL,
    url_medium varchar(255)  NULL,
    url_large varchar(255)  NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    id_property int  NOT NULL,
    CONSTRAINT images_pk PRIMARY KEY (image_id)
);

-- Table: message
CREATE TABLE message (
    id_message serial  NOT NULL,
    body text  NOT NULL,
    id_chat int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    receiver_id int  NOT NULL,
    date timestamp  NOT NULL,
    CONSTRAINT message_pk PRIMARY KEY (id_message)
);

-- Table: people
CREATE TABLE people (
    person_id serial  NOT NULL,
    first_name varchar(100)  NOT NULL,
    second_name varchar(100)  NULL,
    third_name varchar(100)  NULL,
    last_name varchar(100)  NOT NULL,
    email_1 varchar  NULL,
    phoneNumber_1 varchar  NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT people_pk PRIMARY KEY (person_id)
);

-- Table: privileges
CREATE TABLE privileges (
    privilege_id serial  NOT NULL,
    privilege_name varchar(30)  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT privileges_pk PRIMARY KEY (privilege_id)
);

-- Table: property
CREATE TABLE property (
    id_property serial  NOT NULL,
    price money  NOT NULL,
    id_transaction_type int  NOT NULL,
    address varchar(30)  NOT NULL,
    comment text  NOT NULL,
    sale_state boolean  NOT NULL,
    id_property_type int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT property_pk PRIMARY KEY (id_property)
);

-- Table: property_type
CREATE TABLE property_type (
    id_property_type serial  NOT NULL,
    name varchar(30)  NOT NULL,
    CONSTRAINT property_type_pk PRIMARY KEY (id_property_type)
);

-- Table: role_privileges
CREATE TABLE role_privileges (
    role_privi_id serial  NOT NULL,
    role_id int  NOT NULL,
    privilege_id int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT role_privileges_pk PRIMARY KEY (role_privi_id)
);

-- Table: role_user
CREATE TABLE role_user (
    role_user_id serial  NOT NULL,
    user_id int  NOT NULL,
    role_id int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT role_user_pk PRIMARY KEY (role_user_id)
);

-- Table: roles
CREATE TABLE roles (
    role_id serial  NOT NULL,
    role_name varchar(30)  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT roles_pk PRIMARY KEY (role_id)
);

-- Table: transaction_type
CREATE TABLE transaction_type (
    id_transaction_type serial  NOT NULL,
    name varchar(30)  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT transaction_type_pk PRIMARY KEY (id_transaction_type)
);

-- Table: trip_review
CREATE TABLE trip_review (
    id_review serial  NOT NULL,
    comment text  NULL,
    stars decimal(10,4)  NOT NULL,
    date timestamp  NOT NULL,
    assignment_id_assignment int  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT trip_review_pk PRIMARY KEY (id_review)
);

-- Table: users
CREATE TABLE users (
    user_id serial  NOT NULL,
    person_id int  NOT NULL,
    name varchar(100)  NOT NULL,
    email varchar(100)  NOT NULL,
    password varchar(255)  NOT NULL,
    remeber_token varchar(100)  NOT NULL,
    created_at timestamp  NOT NULL,
    updated_at timestamp  NULL,
    deleted_at timestamp  NULL DEFAULT null,
    CONSTRAINT users_pk PRIMARY KEY (user_id)
);

-- foreign keys
-- Reference: Role_Privi_Privilege (table: role_privileges)
ALTER TABLE role_privileges ADD CONSTRAINT Role_Privi_Privilege
    FOREIGN KEY (privilege_id)
    REFERENCES privileges (privilege_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Role_Privi_Role (table: role_privileges)
ALTER TABLE role_privileges ADD CONSTRAINT Role_Privi_Role
    FOREIGN KEY (role_id)
    REFERENCES roles (role_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Role_User_Role (table: role_user)
ALTER TABLE role_user ADD CONSTRAINT Role_User_Role
    FOREIGN KEY (role_id)
    REFERENCES roles (role_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Role_User_user (table: role_user)
ALTER TABLE role_user ADD CONSTRAINT Role_User_user
    FOREIGN KEY (user_id)
    REFERENCES users (user_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: agent_estate_company (table: agent_estate)
ALTER TABLE agent_estate ADD CONSTRAINT agent_estate_company
    FOREIGN KEY (id_company)
    REFERENCES company (id_company)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: agent_estate_people (table: agent_estate)
ALTER TABLE agent_estate ADD CONSTRAINT agent_estate_people
    FOREIGN KEY (person_id)
    REFERENCES people (person_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: assignment_agent_estate (table: assignment)
ALTER TABLE assignment ADD CONSTRAINT assignment_agent_estate
    FOREIGN KEY (agent_estate_id_agent)
    REFERENCES agent_estate (id_agent)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: assignment_client (table: assignment)
ALTER TABLE assignment ADD CONSTRAINT assignment_client
    FOREIGN KEY (client_id_client)
    REFERENCES client (id_client)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: buy_review_client (table: buy_review)
ALTER TABLE buy_review ADD CONSTRAINT buy_review_client
    FOREIGN KEY (id_client)
    REFERENCES client (id_client)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: buy_review_property (table: buy_review)
ALTER TABLE buy_review ADD CONSTRAINT buy_review_property
    FOREIGN KEY (id_property)
    REFERENCES property (id_property)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: chat_listener (table: chat)
ALTER TABLE chat ADD CONSTRAINT chat_listener
    FOREIGN KEY (listener_id)
    REFERENCES users (user_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: chat_me (table: chat)
ALTER TABLE chat ADD CONSTRAINT chat_me
    FOREIGN KEY (session_id)
    REFERENCES users (user_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: client_company (table: client)
ALTER TABLE client ADD CONSTRAINT client_company
    FOREIGN KEY (id_company)
    REFERENCES company (id_company)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: client_people (table: client)
ALTER TABLE client ADD CONSTRAINT client_people
    FOREIGN KEY (person_id)
    REFERENCES people (person_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: images_property (table: images)
ALTER TABLE images ADD CONSTRAINT images_property
    FOREIGN KEY (id_property)
    REFERENCES property (id_property)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: message_chat (table: message)
ALTER TABLE message ADD CONSTRAINT message_chat
    FOREIGN KEY (id_chat)
    REFERENCES chat (id_chat)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: message_users (table: message)
ALTER TABLE message ADD CONSTRAINT message_users
    FOREIGN KEY (receiver_id)
    REFERENCES users (user_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: property_property_type (table: property)
ALTER TABLE property ADD CONSTRAINT property_property_type
    FOREIGN KEY (id_property_type)
    REFERENCES property_type (id_property_type)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: property_transaction_type (table: property)
ALTER TABLE property ADD CONSTRAINT property_transaction_type
    FOREIGN KEY (id_transaction_type)
    REFERENCES transaction_type (id_transaction_type)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: trip_review_assignment (table: trip_review)
ALTER TABLE trip_review ADD CONSTRAINT trip_review_assignment
    FOREIGN KEY (assignment_id_assignment)
    REFERENCES assignment (id_assignment)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: users_people (table: users)
ALTER TABLE users ADD CONSTRAINT users_people
    FOREIGN KEY (person_id)
    REFERENCES people (person_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.
