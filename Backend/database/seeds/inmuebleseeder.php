<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\inmueblem;

class inmuebleseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $itt=DB::table('transaccion_type')->select('id_transaccion_type')->first();
        $ipt=DB::table('property_type')->select('id_property_type')->first();

        
    //    DB::table('property')->insert([
    //        'price'=>'300000.00',
    //        'id_transaccion_type'=>$itt->id_transaccion_type,
    //        'address'=>'Achumani',
    //        'comment'=>'hermosa casa!',
    //        'sale_state'=>'1',
    //        'id_property_type'=>$ipt->id_property_type,
    //    ]); 
    

    inmueblem::create([
        'price'=>'300000.00',
        'id_transaccion_type'=>$itt->id_transaccion_type,
        'address'=>'Achumani',
        'comment'=>'hermosa casa!',
        'sale_state'=>'1',
        'id_property_type'=>$ipt->id_property_type,
    ]);
    
    }
}
