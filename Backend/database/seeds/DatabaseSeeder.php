<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $this->truncatetables([
            'transaccion_type',
            'property_type',
            'property',
            'company'
        ]);
        
        $this->call([proptype::class,transt::class,inmuebleseeder::class,CompanySeeder::class,usuarios::class]);
    }

    
    public function truncatetables(array $tables)
    {

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

    }
}
