<?php

use App\Company;
use App\inmueblem;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run(){
        Company::create([
            'company_name' => 'don pollo',
            'manager_name' => 'carlos ms',
            'company_address' => 'abc@abc.com',
            'company_phone' => '808080808',
        ]);
    }
}

