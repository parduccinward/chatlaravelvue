<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function($table) {
            $table->increments('person_id');
            $table->string('first_name',100);
            $table->string('second_name',100)->nullable();
            $table->string('third_name',100)->nullable();
            $table->string('last_name',100);
            $table->string('email',100)->nullable();
            $table->integer('phone_number')->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
