<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbClient extends Migration{
    public function up(){
        Schema::create("client", function(Blueprint$table) {
            $table->increments("id_client");
            $table->unsignedInteger('person_id');
            $table->foreign('person_id')->references('person_id')->on('people');
            $table->boolean('disease')->unsigned()->nullable();
            $table->timestamps();
            $table->softdeletes();
            $table->unsignedInteger('id_company');
            $table->foreign('id_company')->references('id_company')->on('company');

        });
    }
    public function down(){
        Schema::drop("client");
    }
}
