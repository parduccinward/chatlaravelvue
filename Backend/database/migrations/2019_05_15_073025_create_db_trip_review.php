<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBTripReview extends Migration
{
    public function up(){

        Schema::create("trip_review", function(Blueprint$table) {
           $table->increments("id_review");
            $table->string('comment')->unsigned()->nullable();
            $table->decimal('stars',8,2)->unsigned()->nullable();
            $table->date('date');
            $table->integer('assignment_id_assignment');
            //$table->integer('assignment_id_assignment')->unsigned()->nullable();
            $table->timestamps();
            $table->softdeletes();

            //$table->foreign('assignment_id_assignment')->references('id_assignment')->on('assignment');
        });
    }
    public function down(){
        Schema::dropIfExists("trip_review");
    }
}
