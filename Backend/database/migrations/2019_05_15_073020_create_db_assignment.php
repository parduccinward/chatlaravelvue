<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbAssignment extends Migration{
    public function up(){
        Schema::create("assignment", function(Blueprint$table) {
            $table->increments("id_assignment");
            $table->date('date');
            $table->unsignedInteger('id_agent');
            $table->foreign('id_agent')->references('id_agent')->on('agentestate');
            $table->unsignedInteger('id_client');
            $table->foreign('id_client')->references('id_client')->on('client');
            $table->unsignedInteger('id_review');
            $table->timestamps();
            $table->softdeletes();
        });
    }
    public function down(){
        Schema::drop("assignment");
    }
}