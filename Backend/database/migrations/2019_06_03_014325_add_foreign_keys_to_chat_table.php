<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('chat', function(Blueprint $table)
		{
			$table->foreign('listener_id', 'chat_listener')->references('user_id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('session_id', 'chat_me')->references('user_id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('chat', function(Blueprint $table)
		{
			$table->dropForeign('chat_listener');
			$table->dropForeign('chat_me');
		});
	}

}
