<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBBuyReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create("buy_review", function($table) {
            $table->increments("id_review");
            $table->string('comment')->unsigned()->nullable();
            $table->decimal('starts',8,2)->unsigned()->nullable();
            $table->integer('id_client')->unsigned()->nullable();
            $table->timestamp('date')->unsigned()->nullable();
            $table->integer('id_property')->unsigned()->nullable();

            $table->timestamps();
            $table->softdeletes();

            $table->foreign('id_client')->references('id_client')->on('client');
            $table->foreign('id_property')->references('id_property')->on('property');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("buy_review");
    }
}
