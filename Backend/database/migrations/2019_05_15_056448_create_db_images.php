<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function($table) {
            $table->increments('image_id');
            $table->string('filename',255)->nullable();
            $table->string('url_original',255);
            $table->string('url_little',255)->nullable();
            $table->string('url_medium',255)->nullable();
            $table->string('url_large',255)->nullable();
            $table->unsignedInteger('id_property');
            $table->foreign('id_property')->references('id_property')->on('property');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
