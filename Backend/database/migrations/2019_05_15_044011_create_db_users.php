<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function($table) {
            $table->increments('user_id');
            $table->unsignedInteger('person_id')->unsigned()->nullable();
            $table->foreign('person_id')->references('person_id')->on('people');
            $table->string('name',255);
            $table->string('email',100);
            $table->string('password'); //cambiar despues
            $table->string('remember_token', 100)->unsigned()->nullable();
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
