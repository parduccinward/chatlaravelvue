<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbAgentstate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agentestate', function(Blueprint$table) {
            $table->increments('id_agent');
            $table->unsignedInteger('id_company');
            $table->foreign('id_company')->references('id_company')->on('company');
            $table->boolean('car');
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agentestate');
    }
}
