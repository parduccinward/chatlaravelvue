<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbAssignmentchat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignmentchat', function($table) {
            $table->increments('id_assignement_chat');
            $table->unsignedInteger('id_chat');
            $table->foreign('id_chat')->references('id_chat')->on('chat');
            $table->unsignedInteger('id_assignment');
            $table->foreign('id_assignment')->references('id_assignment')->on('assignment');
            $table->timestamps();
            $table->softdeletes();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('assignmentchat');
    }
}
