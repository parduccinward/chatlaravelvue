<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDbProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function(Blueprint $table) {
            $table->increments('id_property');

            $table->decimal('price',9,2);
            $table->unsignedBigInteger('id_transaccion_type');
            $table->foreign('id_transaccion_type')->references('id_transaccion_type')->on('transaccion_type')->ondelete('cascade');
            $table->string('address',30);
            $table->string('comment',100);
            $table->boolean('sale_state')->default(false);
            $table->unsignedBigInteger('id_property_type');
            $table->foreign('id_property_type')->references('id_property_type')->on('property_type')->ondelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property');
    }
}
