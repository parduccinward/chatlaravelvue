@extends('layouts.app')

@section('content')
<!-- desde aqui es la pagina de inicio -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pagina De Inicio Real State</title>
</head>
<body>
    <h1>Menu Principal</h1>
    <div class="flex-center position-ref full-height">
            <div class="top-right links">

                    {{--
                    <a href="{{ route('inmuebles') }}">Inmuebles</a>
                    --}}
                    <a href="{{ route('inmobiliaria') }}">Inmobiliaria</a>

                <a href="{{route ('Trip_Review.list')}}">Trip Review</a>

            </div> 
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>  
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
@endsection
