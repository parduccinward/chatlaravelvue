<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>{{$title}}</h1>
    @unless (empty($inmuebless))
    <ul>
        @foreach ($inmuebless as $inmueble)
            <li>{{ $inmueble }}</li>
        @endforeach
    </ul>
        @else
    <p>No hay inmuebles publicados.</p>
        @endunless
    <div class="flex-center position-ref full-height">
                <div class="top-right links">
                        <a href="{{ route('publicar') }}">Publicar</a>
                        <a href="{{ route('buscar') }}">Buscar</a>
                </div> 
    </div>
</body>
</html>