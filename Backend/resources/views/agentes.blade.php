<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ABM</title>
</head>
<body>
    <h1>Menu de Control de Companias</h1>
   
    <div class="flex-center position-ref full-height">
                <div class="top-right links">
                        <a href="{{ route('registrarcom') }}">Registrar</a>
                        <a href="{{ route('modificarcom') }}">Modificar</a>
                        <a href="{{ route('eliminarcom') }}">Eliminar</a>
                </div> 
    </div>
    <h1>{{$title}}</h1>
    <div class="flex-center position-ref full-height">
                <div class="top-right links">
                        <a href="{{ route('registrar') }}">Registrar</a>
                        <a href="{{ route('modificar') }}">Modificar</a>
                        <a href="{{ route('eliminar') }}">Eliminar</a>
                </div> 
    </div>
    
</body>
</html>