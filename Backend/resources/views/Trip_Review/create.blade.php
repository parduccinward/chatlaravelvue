<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Review</title>
</head>
<body>
<h1 class="page-header">Review del recorrido</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Review
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form" method="post" action="{{ url('Trip_Review/Crear')}}">
                            {{ csrf_field() }}


                            <div class="form-group">

                                <label for="validationServer01">Commentario </label>
                                <input type="text" class="form-control is-valid" id="validationServer01" name="comment" placeholder="Comentario" value="" required>
                                <div class="valid-feedback">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Estrellas</label>
                                <select class="form-control" name="stars">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>


                            <button type="submit" class="btn btn-success">Guardar</button>
                            <button type="reset" class="btn btn-info">Vaciar </button>
</body>
</html>