<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Contacts</h1>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Id </th>
                <th>Comment</th>
                <th>stars</th>
                <th>date</th>
                <th>id_assignment</th>
                <th>created</th>
                <th>updated</th>
                <th>deleted</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Trip_Review as $trip)
                <tr>
                    <td>{{ $trip->id_review}}</td>
                    <td>{{ $trip->comment}}</td>
                    <td>{{ $trip->stars}}</td>
                    <td>{{ $trip->date}}</td>
                    <td>{{ $trip->assignment_id_assignment}}</td>
                    <td>{{ $trip->created_at}}</td>
                    <td>{{ $trip->updated_at}}</td>
                    <td>{{ $trip->deleted_at}}</td>


                    <td><a href="{{ url('/Trip_reviewEditar', $trip->id_review) }}" type="button" class="btn btn-outline btn-success">Editar</a></td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $listar->links() }}
    </div>
    <!-- /.table-responsive -->
</div>