<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Actualizar reviews</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br />
        @endif
        <form method="post" action="{{ route('Trip_Review.update', $trip->id_review) }}">
            @method('PATCH')
            @csrf
            <div class="form-group">

                <label for="first_name">id:</label>
                <input type="text" class="form-control" name="id_review" value={{ $trip->id_review }} />
            </div>

            <div class="form-group">
                <label for="last_name">comentario:</label>
                <input type="text" class="form-control" name="comment" value={{ $trip->comment }} />
            </div>

            <div class="form-group">
                <label for="email">estrellas:</label>
                <input type="text" class="form-control" name="stars" value={{ $trip->stars }} />
            </div>
            <div class="form-group">
                <label for="city">fecha:</label>
                <input type="text" class="form-control" name="date" value={{ $trip->date }} />
            </div>
            <div class="form-group">
                <label for="country">id_assignment:</label>
                <input type="text" class="form-control" name="assignment_id_assignment" value={{ $trip->assignment_id_assignment }} />
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>