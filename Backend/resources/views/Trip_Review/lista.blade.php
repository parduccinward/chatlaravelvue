<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trip_Review</title>
</head>
<body>
<h1>Trip_Review</h1>

<div class="flex-center position-ref full-height">
    <div class="top-right links">
        <a href="{{route ('Trip_Review.create')}}">crear</a>
        <a href="{{route ('Trip_Review.liste')}}">lista</a>
    </div>
</div>
<div class="uper">
    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br />
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <td>ID</td>
            <td>Book Name</td>
            <td>ISBN Number</td>
            <td>Book Price</td>
            <td colspan="2">Action</td>
        </tr>
        </thead>
        <tbody>
        @foreach($Trip_Review as $trip_review)
            <tr>
                <td>{{$trip_review->id_review}}</td>
                <td>{{$trip_review->comment}}</td>
                <td>{{$trip_review->date}}</td>
                <td>{{$trip_review->assignment_id_assignment}}</td>
            <td><a href="{{route('Trip_Review.edit',$trip_review->id_review)}}" class="btn btn-primary">Editar</a></td>
                <td>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div>
</body>
</html>