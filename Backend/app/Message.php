<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_message
 * @property int $id_chat
 * @property int $receiver_id
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $date
 * @property Chat $chat
 * @property User $user
 */
class Message extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'message';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_message';

    /**
     * @var array
     */
    protected $fillable = ['body'];
    protected $appends = ['selfMessage'];

    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getSelfMessageAttribute()
    {
        return $this->user_id === auth()->user()->id;
    }
}
