<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $table = 'company';
    protected $primaryKey = 'id_company';
    protected $fillable =['company_name','manager_name','company_address',
    'company_phone'];
    

/*class Company extends Model
{
    //
    protected $table = 'company';
    protected $primaryKey = 'id_company';

    protected $fillable = ['phone'];

    // Relacion de uno a muchos
    public function agent_estates(){

        // definir clase
        return $this->hasMany('App\Agent_estate');
    }
    // Relacion de uno a muchos
    public function clients(){
        return $this->hasMany('App\Client');
    }*/
}
