<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'client';
    protected $primaryKey = 'id_client';

    protected $fillable = ['person_id',
        'disease',
        'id_company'];


    // Relacion de uno a muchos
    public function buy_reviews(){
        return $this->hasMany('App\Buy_review');
    }
    public function assignments(){
        return $this->hasMany('App\Assignment');
    }


    // Relacion de Muchos a uno
    public function people (){
        return $this->belongsTo('App\People', 'person_id');
    }

}
