<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transacciont extends Model
{
    protected $table = 'transaccion_type';
    protected $primaryKey = 'id_transaccion_type';
    
    protected $fillable = ['name'];

}
