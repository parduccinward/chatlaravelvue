<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Trip_Review extends Model
{
    protected $table = 'trip_review';
    protected $fillable = [
        'comment',
        'stars',
        'date',
        'assignment_id_assignment'
    ];
    protected $primaryKey='id_review';
}