<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class imagenes extends Model
{
    use SoftDeletes;
    protected $table = 'images';
    protected $primaryKey = 'image_id';

    protected $fillable = ['url_original','id_property']; 
    
    public function inmuebless()
    {
    return $this->belongsTo(inmueblem::class,'id_property');
    }

}
