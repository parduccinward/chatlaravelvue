<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_assignment_chat
 * @property int $id_assignment
 * @property int $id_chat
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Assignment $assignment
 * @property Chat $chat
 */
class Assignment_chat extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'assignment_chat';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_assignment_chat';

    /**
     * @var array
     */
    protected $fillable = ['id_assignment', 'id_chat', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignment()
    {
        return $this->belongsTo('App\Assignment', 'id_assignment', 'id_assignment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo('App\Chat', 'id_chat', 'id_chat');
    }
}
