<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class inmueblem extends Model
{
    use SoftDeletes;
    protected $table = 'property';
    protected $primaryKey = 'id_property';

    protected $fillable = ['price','id_transaccion_type','address',
    'comment','sale_state','id_property_type'];

    public function transaccion()
    {
    return $this->belongsTo(transacciont::class,'id_transaccion_type');
    }  

    public function property()
    {
    return $this->belongsTo(propertyt::class,'id_property_type');
    }  
}
