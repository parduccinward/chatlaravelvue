<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_assignment
 * @property int $agent_estate_id_agent
 * @property int $client_id_client
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property AgentEstate $agentEstate
 * @property Client $client
 * @property AssignmentChat[] $assignmentChats
 * @property TripReview[] $tripReviews
 */
class Assignment extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'assignment';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_assignment';

    /**
     * @var array
     */
    protected $fillable = ['agent_estate_id_agent', 'client_id_client', 'date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agentEstate()
    {
        return $this->belongsTo('App\AgentEstate', 'agent_estate_id_agent', 'id_agent');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id_client', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignmentChats()
    {
        return $this->hasMany('App\AssignmentChat', 'id_assignment', 'id_assignment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tripReviews()
    {
        return $this->hasMany('App\TripReview', 'assignment_id_assignment', 'id_assignment');
    }
}
