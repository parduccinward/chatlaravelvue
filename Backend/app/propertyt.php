<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class propertyt extends Model
{
    protected $table = 'property_type';
    protected $primaryKey = 'id_property_type';
    protected $fillable = ['name'];
}
