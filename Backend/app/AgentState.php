<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_agent
 * @property int $id_company
 * @property boolean $car
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Company $company
 * @property Assignment[] $assignments
 */
class AgentState extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'agent_estate';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_agent';

    /**
     * @var array
     */
    protected $fillable = ['id_company', 'car', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Company', 'id_company', 'id_company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignments()
    {
        return $this->hasMany('App\Assignment', 'agent_estate_id_agent', 'id_agent');
    }
}
