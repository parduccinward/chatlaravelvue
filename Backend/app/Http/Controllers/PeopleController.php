<?php

namespace App\Http\Controllers;

use App\Http\Bl\PropertyLogic;
use App\People;
use Illuminate\Http\Request;
use App\Http\Bl\PeopleLogic;
use Validator;

class PeopleController extends Controller
{

//    public function __construct(){
//        $this->middleware('jwt');
//    }
    // listado de todos las personas o busqueda
    public function index($search = null){
        $people = new PeopleLogic();
        if($search) {
            return $people->SearchLog($search);
        }else{
            return $people->IndexLog();
        }

    }


    public function create(){
        // para vista
    }


    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:100',
            'second_name' => 'string|max:100|required',
            'third_name' => 'string|max:100|required',
            'last_name' => 'required|string|max:100',
            'email' => 'required|string|email|max:100|unique:users',
            'phone_number' => 'required|numeric',
        ]);

        if($validator->fails()){
            //return response()->json($validator->errors()->toJson(), 400);

            //correcto
            return response()->json($validator->errors(), 400);
        }

        $people = new PeopleLogic();
        return $people->StoreLog($request);
    }


    public function show($id){
        // para vista
    }


    public function edit($id){
        // para vista
    }


    public function update(Request $request, $person_id){
        $validator = Validator::make($request->all(), [
            'first_name' => 'string|max:100',
            'second_name' => 'string|max:100|required',
            'third_name' => 'string|max:100|required',
            'last_name' => 'string|max:100|required',
            'email' => 'required|string|email|max:100|unique:users',
            'phone_number' => 'required|numeric',
        ]);
        if($validator->fails() || !(isset($person_id))){
            //return response()->json($validator->errors()->toJson(), 400);

            // En formato json correcto
            return response()->json($validator->errors(), 400);
        }
        $people = new PeopleLogic();
        return $people->UpdateLog($request,$person_id);

    }
    public function destroy($person_id){
        $people = new PeopleLogic();
        return $people->DeleteLog($person_id);
    }

}
