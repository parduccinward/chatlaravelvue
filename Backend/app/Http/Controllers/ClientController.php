<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Bl\ClientLogic;
use Illuminate\Http\Request;
use Validator;

class ClientController extends Controller
{
    public function index(){
        $client = new ClientLogic();
        return $client->IndexLog();
    }

    public function create(){
        // para la vista
    }


    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'person_id' => 'required|numeric',
            'disease' => 'required',
            'id_company' => 'required|numeric',
        ]);

        if($validator->fails()){
            //return response()->json($validator->errors()->toJson(), 400);
            return response()->json($validator->errors(), 400);
        }
        $client = new ClientLogic();
        return $client->StoreLog($request);
    }


    public function show($id){
        // para la vista
    }


    public function edit($id){
        // para la vista
    }


    public function update(Request $request, $id_client){

        $validator = Validator::make($request->all(), [
            'person_id' => 'numeric',
            'disease' => 'required',
            'id_company' => 'required|numeric',
        ]);

        if($validator->fails() || !isset($id_client)){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $client = new ClientLogic();
        return $client->UpdateLog($request,$id_client);
    }


    public function destroy($id_client){
        $client = new ClientLogic();
        return $client->DeleteLog($id_client);
    }
}
