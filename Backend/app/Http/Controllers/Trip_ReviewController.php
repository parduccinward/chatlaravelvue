<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\trip_review;
use App\Http\Bl\Trip_ReviewLogic;
use App\Notifications\TripReviewError;
use App\Notifications\TripReviewFatal;
use App\Notifications\TripReviewWarn;
use App\User;
use Illuminate\Support\Facades\Notification;
use Validator;

class Trip_ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $trip_review = new Trip_ReviewLogic();
        return $trip_review->ShowLog();
        //$title = 'Trip Review1';
        //$Trip_Review = trip_review::all();

        //return view('Trip_Review.lista', compact('title','Trip_Review'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|string|max:255',
            'stars' => 'required|numeric|min:1|max:5',
            'date' => 'required|date',
            'assignment_id_assignment' => 'required|numeric',
        ]);
        //
        if($validator->fails()){
            Notification::route('mail', 'asd@ejemplisho.com')->notify(new TripReviewFatal());
            return response()->json($validator->errors(), 400);

        }

        $trip_review = new Trip_ReviewLogic();
        return $trip_review->StoreTrip($request);
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function show()
    {
        $trip_review = new Trip_ReviewLogic();
        Notification::route('mail', 'czczyasxczc@ejemplisho.com')->notify(new TripReviewError());
        return $trip_review->ShowLog();
        //$trip_review = trip_review::all();
        //return response()->json($trip_review);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip = trip_review::findOrFail($id);

        return view('Trip_Review.edit', compact('trip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
