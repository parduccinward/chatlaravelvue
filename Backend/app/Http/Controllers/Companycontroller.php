<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Validator;

class Companycontroller extends Controller
{
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'company_name' => 'required|string|max:255',
            'manager_name' => 'required|string|max:255',
            'company_address' => 'required|max:255',
            'company_phone' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }



        $companys = new Company ();

        $companys->company_name = $request->input('company_name');
        $companys->manager_name = $request->input('manager_name');
        $companys->company_address = $request->input('company_address');
        $companys->company_phone = $request->input('company_phone');

        $companys->save();
        return response()->json($companys);
    }

    public function show(){
        $companys = Company::all();
        return response()->json($companys);
    }

    public function showid($id){
        $companys = Company::find($id);
        return response()->json($companys);
    }

    public function updateid(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'company_name' => 'required|string|max:255',
            'manager_name' => 'required|string|max:255',
            'company_address' => 'required|max:255',
            'company_phone' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }


        $companys = Company::find($id);
        $companys->company_name = $request->input('company_name');
        $companys->manager_name = $request->input('manager_name');
        $companys->company_address = $request->input('company_address');
        $companys->company_phone = $request->input('company_phone');

        $companys->save();
        return response()->json($companys);
    }

    public function deleteid(Request $request, $id){
        


        $companys = Company::find($id);
        $companys->delete();
        return response()->json($companys);
    }

}
