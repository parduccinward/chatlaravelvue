<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function manageChat(){
        $listener = AgentState::select('users.user_id')
        ->where('id_agent','=','1')   //ESTO DEBERIA SER INTEGRADO DE OTRO LUGAR
        ->join('people','people.person_id', '=', 'agent_estate.person_id')
        ->join('users','users.person_id', '=', 'people.person_id')
        ->get();            
        $chat = Chat::where([['session_id', '=', 1],['listener_id', '=',current($listener->toArray())]])->first();
        //dd($chat);
        //Auth::user()->user_id
            //$listener->user_id)
        if($chat){ // Si no esta vacio do this
            $messages = Message::select('body')->where('id_chat','=',$chat->id_chat)->get();
            // foreach ($messages as $message) {
            //     dd($chat->id_chat);
            // }
            return response()->json($messages);
        }else{
            $date = Carbon\Carbon::now();
            $chat = new Chat([
                'date'=> $date,
                'created_at'=> $date,
                'updated_at'=> $date,
                'deleted_at'=> $date,
                'listener_id'=> implode(current($listener->toArray())),
                'session_id'=> 1,
              ]);
            $chat->save();
        }
        
     }

    public function newMessage(Request $request){
        $listener = AgentState::select('users.user_id')
        ->where('id_agent','=','1')   //ESTO DEBERIA SER INTEGRADO DE OTRO LUGAR
        ->join('people','people.person_id', '=', 'agent_estate.person_id')
        ->join('users','users.person_id', '=', 'people.person_id')
        ->get();            
        $chat = Chat::where([['session_id', '=', 1],['listener_id', '=',current($listener->toArray())]])->first();
        
        //Auth::user()->user_id)
        $validator = Validator::make($request->all(), [
            'body' => 'required|string|max:255',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $message = new Message();
        $message->body = $request->input('body');
        $message->id_chat = $chat->id_chat;
        $message->created_at = null;
        $message->updated_at = null;
        $message->deleted_at = null;
        $message->receiver_id = implode(current($listener->toArray()));
        $message->date = Carbon\Carbon::now();
        $message->save();
        return response()->json($message);
    }

}
