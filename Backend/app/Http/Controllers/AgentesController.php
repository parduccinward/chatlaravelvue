<?php

namespace App\Http\Controllers;

use App\Agentes;
use Illuminate\Http\Request;

class AgentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Menu de Control de Agentes Inmobiliarios';
        return view('agentes', compact('title'));
        
  
    }
    public function registrar(){

        return view('agentescreate');
    }

    public function modificar(){

        return view('agentesedit');
    }

    public function eliminar(){

        return view('agentesdelete');
    }

    public function registrarcom(){

        return view('comcreate');
    }

    public function modificarcom(){

        return view('comedit');
    }

    public function eliminarcom(){

        return view('comdelete');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $data = request()->all();
        Agentes::create(
            ['phone'=>$data['telefono']
            
            ]);
        return redirect()->route('inmobiliaria');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agentes  $agentes
     * @return \Illuminate\Http\Response
     */
    public function show(Agentes $agentes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agentes  $agentes
     * @return \Illuminate\Http\Response
     */
    public function edit(Agentes $agentes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agentes  $agentes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agentes $agentes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agentes  $agentes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agentes $agentes)
    {
        //
    }
}
