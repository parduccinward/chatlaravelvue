<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Bl\PropertyLogic;
use App\inmueblem;
use App\imagenes;
use Validator;

class InmueblesController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'price' => 'required|numeric|between:1,9999999',
            'id_transaccion_type' => 'required|numeric|exists:transaccion_type,id_transaccion_type',
            'comment' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'sale_state' => 'required|boolean',
            'id_property_type' => 'required|numeric|exists:property_type,id_property_type',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }else{
            $class = new PropertyLogic();
            return $class->Storelog($request);
        }
    }

    public function buscador(Request $request){
        $validator = Validator::make($request->all(), [
            'price' => 'numeric|nullable|between:1,9999999',
            'id_transaccion_type' => 'numeric|nullable|exists:transaccion_type,id_transaccion_type',
            'address' => 'string|max:255|nullable',
            'id_property_type' => 'numeric|nullable|exists:property_type,id_property_type',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }else{
            $class = new PropertyLogic();
            return $class->Searchlog($request);
        }
    }

    public function show(){
        $class = new PropertyLogic();
        return $class->Showlog();
    }

    public function imasave(Request $request){
        $validator = Validator::make($request->all(), [
            'casaima'=>'required|image',
            'id_property' => 'required|numeric|exists:property,id_property',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }else{
            $class = new PropertyLogic();
            return $class->Storeimagelog($request);
        }
    }

    public function borr(){
    Storage::deleteDirectory('fotocasas');
    return 'Eliminado ';
    }

    public function showima(){
        $class = new PropertyLogic();
        return $class->Showimageslog();
    }
    public function UpdateProperty(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'price' => 'required|numeric|between:1,9999999',
            'id_transaccion_type' => 'required|numeric|exists:transaccion_type,id_transaccion_type',
            'comment' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'sale_state' => 'required|boolean',
            'id_property_type' => 'required|numeric|exists:property_type,id_property_type',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }else{
                $class = new PropertyLogic();
                return $class->UpdatePropertylog($request, $id);
        }
    }
    
    public function DeleteProperty($id){
            $class = new PropertyLogic();
            return $class->DeletePropertylog($id);
    }

    public function DeleteImage($id){
            $class = new PropertyLogic();
            return $class->DeleteImagelog($id);
    }
}
