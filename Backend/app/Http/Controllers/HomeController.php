<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // Esto verifica que el usuario este indentificado 
    // solo es valido para este controlador 
    // agregar esta funcion al controlador que necesite un usuario logeado
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // Muestra la applicacion en el dashboard.
    public function index()
    {
        // esto se refiere a la vista dentro de la carpeta
        // resources/views/home.blade.php
        return view('home');
    }
}
