<?php


namespace App\Http\DAO;


use App\Client;
use App\Company;
use App\Http\Bl\PeopleLogic;
use App\People;
use Illuminate\Http\Request;

class DAOClient
{
    public function IndexClient(){
        $client = Client::all();
        return response()->json($client);
    }
    public function StoreClient(Request $request){
        $client = new Client();
        $client->person_id = $request->input('person_id');
        $client->disease = $request->input('disease');
        $client->id_company = $request->input('id_company');
        if(People::find($client->person_id) && Company::find($client->id_company)){
            $client->save();
            return response()->json($client);
        }else{
            return response()->json('no hay persona o compania!!');
        }
    }

    public function UpdateClient(Request $request, $id_client){
        if (is_int((int)$id_client)) {
            // conseguir al cliente
            $client = Client::find($id_client);
            if($client) {
                $client->person_id = $request->input('person_id');
                $client->disease = $request->input('disease');
                $client->id_company = $request->input('id_company');
                if(People::find($client->person_id) && Company::find($client->id_company)) {
                    $client->update();
                    return response()->json($client);
                }else{
                    return response()->json('no hay persona o compania!!');
                }

            }else{
                return response()->json('Cliente no eliminado!!');
            }
        }else{
            return response()->json('Debe ingresar campo correcto');
        }
    }
    public function DeleteClient($id_client){
        if (is_int((int)$id_client)) {
            $client = Client::find($id_client);
            if ($client) {
                Client::find($id_client)->delete();
                return response()->json("Clinte eliminado!!");
            } else {
                return response()->json('Cliente no encontrado!!');
            }
        }else{
            return response()->json('Debe ingresar campo correcto');
        }
    }
}
