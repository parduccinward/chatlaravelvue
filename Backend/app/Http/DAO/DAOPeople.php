<?php

namespace App\Http\DAO;

use App\People;
use Illuminate\Http\Request;

class DAOPeople
{
    public function IndexPeople(){
        $people = People::all();
        return response()->json($people);
    }
    public function SearchPeople($search){
        $people = People::where('first_name','LIKE','%'.$search.'%')
            ->orWhere('second_name','LIKE','%'.$search.'%')
            ->orWhere('third_name','LIKE','%'.$search.'%')
            ->orWhere('last_name','LIKE','%'.$search.'%')
            ->orderBy('person_id','desc')
            ->get();
        if($people && count($people)>=1){
            return response()->json($people);
        }else{
            return response()->json('No hay personas con '.$search);
        }
    }
    public function StorePeople(Request $request){
        $people = new People();
        $people->first_name = $request->input('first_name');
        $people->second_name = $request->input('second_name');
        $people->third_name = $request->input('third_name');
        $people->last_name = $request->input('last_name');
        $people->email = $request->input('email');
        $people->phone_number = $request->input('phone_number');
        $people->save();
        return response()->json($people);
    }

    public function UpdatePeople(Request $request, $person_id){
        if (is_int((int)$person_id)){
            //conseguir a la persona
            $people = People::find($person_id);
            if($people) {
                $people->first_name = $request->input('first_name');
                $people->second_name = $request->input('second_name');
                $people->third_name = $request->input('third_name');
                $people->last_name = $request->input('last_name');
                $people->email = $request->input('email');
                $people->phone_number = $request->input('phone_number');
                $people->update();
                return response()->json($people);
            }else{
                return response()->json('Persona no encontrada!!');
            }
        }else{
            return response()->json('Debe ingresar campo correcto');
        }
    }
    public function DeletePeople($person_id){

        if (is_int((int)$person_id)) {
            //conseguir a la persona
            $people = People::find($person_id);
            if ($people) {
                People::find($person_id)->delete();
                return response()->json("Persona eliminada!!");
            } else {
                return response()->json('Persona no encontrada!!');
            }
        }else{
            return response()->json('Debe ingresar campo correcto');
        }
    }
}
