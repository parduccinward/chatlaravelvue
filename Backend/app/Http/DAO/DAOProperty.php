<?php

namespace App\Http\DAO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\inmueblem;
use App\imagenes;

class DAOProperty
{

    public function StorageProperty(Request $request){
        $inmuebles = new inmueblem ();
        
       $inmuebles->price = $request->input('price');
       $inmuebles->id_transaccion_type = $request->input('id_transaccion_type'); 
       $inmuebles->address = $request->input('address'); 
       $inmuebles->comment = $request->input('comment'); 
       $inmuebles->sale_state = $request->input('sale_state');
       $inmuebles->id_property_type = $request->input('id_property_type'); 
       
       $inmuebles->save(); 
       return response()->json($inmuebles);
    }

    public function SearchProperty(Request $request){
        $inmuebles = new inmueblem ();

        if ($request->input('price') != null) {
            $inmuebles = $inmuebles->where('price','<=', $request->input('price'));
        }
        if ($request->input('id_transaccion_type')!= null) {
            $inmuebles = $inmuebles->where('id_transaccion_type', $request->input('id_transaccion_type'));
        }        
        if ($request->input('address')!= null) {
            $inmuebles = $inmuebles->where('address', $request->input('address'));
        }
        if ($request->input('id_property_type')!= null) {
            $inmuebles = $inmuebles->where('id_property_type', $request->input('id_property_type'));
        }

        $inmuebles = $inmuebles->get();
        if ($inmuebles->isEmpty()== TRUE) { 
            return 'No se encontraron Casas con las especificaciones puestas';
           }else{ 
        return response()->json($inmuebles);
           }    
    }

    public function ShowProperty(){
    $inmuebles = inmueblem::all();
    //$inmuebles = inmueblem::onlyTrashed()->get();
    return response()->json($inmuebles);
    }

    public function StorageImage(Request $request){
        $imagencas = new imagenes ();      
        $path = Storage::putFile('fotocasas', $request->file('casaima'),'public');
        $imagencas->url_original =$path; 
        $imagencas->id_property = $request->input('id_property');
        $imagencas->save();
        return 'Imagen Cargada Satisfactoriamente en : '.$path;
    }

    public function ShowImages(){
        $imagenes = imagenes::all();
    //    $imagenes = imagenes::onlyTrashed()->get();
        return response()->json($imagenes);
        }

    public function UpdatePropertyDao(Request $request, $id){
        $inmuebles = inmueblem::find($id);
       if($inmuebles){ 
       $inmuebles->price = $request->input('price');
       $inmuebles->id_transaccion_type = $request->input('id_transaccion_type'); 
       $inmuebles->address = $request->input('address'); 
       $inmuebles->comment = $request->input('comment'); 
       $inmuebles->sale_state = $request->input('sale_state');
       $inmuebles->id_property_type = $request->input('id_property_type'); 
       
       $inmuebles->save(); 
       return response()->json($inmuebles);
        }else{
        return response()->json('Inmueble no encontrado');
        }
    }
    public function DeletePropertyDao($id){
        $inmuebles = inmueblem::destroy($id);
        if ($inmuebles){
        return response()->json('Inmueble Eliminado satisfactoriamente');
        }else{
            return response()->json('Inmueble no encontrado');
        }
    }

    public function DeleteImageDao($id){
        $imagen = imagenes::destroy($id);
        if($imagen){
            return response()->json('Imagen Eliminado satisfactoriamente');
        }else{
            return response()->json('Imagen no encontrada');
        }
    } 
}