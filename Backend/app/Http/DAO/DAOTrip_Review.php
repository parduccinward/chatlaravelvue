<?php

namespace App\Http\DAO;

use App\Trip_Review;
use Illuminate\Http\Request;

class DAOTrip_Review
{
    public function ShowTrip_Review(){
        $Trip_Review = trip_review::all();
        return response()->json($Trip_Review);
    }
    public function StoreReview(Request $request){

        $trip_review = new trip_review ();
        $trip_review->comment = $request->input('comment');
        $trip_review->stars = $request->input('stars');
        $trip_review->date = $request->input('date');
        $trip_review->assignment_id_assignment = $request->input('assignment_id_assignment');
        $trip_review->save();
        return response()->json($trip_review);
    }

}