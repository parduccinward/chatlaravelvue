<?php

namespace App\Http\Bl;

use App\Notifications\TripReviewWarn;
use Illuminate\Http\Request;
use App\Http\DAO\DAOTrip_Review;
use Illuminate\Support\Facades\Notification;

class Trip_ReviewLogic
{
    public function ShowLog(){
        Notification::route('mail', 'yasress@ejemplisho.com')->notify(new TripReviewWarn('a'));
        $class = new DAOTrip_Review();
        return $class->ShowTrip_Review();
    }
    public function StoreTrip(Request $request){
        $class = new DAOTrip_Review();
        return $class->StoreReview($request);
    }


}