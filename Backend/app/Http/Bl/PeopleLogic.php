<?php

namespace App\Http\Bl;

use Illuminate\Http\Request;
use App\Http\DAO\DAOPeople;

class PeopleLogic{
    public function IndexLog(){
        $class = new DAOPeople();
        return $class->IndexPeople();
    }
    public function SearchLog($search){
        $class = new DAOPeople();
        return $class->SearchPeople($search);
    }
    public function StoreLog(Request $request){
        $class = new DAOPeople();
        return $class->StorePeople($request);
    }
    public function UpdateLog(Request $request,$person_id){
        $class = new DAOPeople();
        return $class->UpdatePeople($request,$person_id);
    }
    public function DeleteLog($person_id){
        $class = new DAOPeople();
        return $class->DeletePeople($person_id);
    }
}
