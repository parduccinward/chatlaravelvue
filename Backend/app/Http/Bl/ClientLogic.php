<?php

namespace App\Http\Bl;

use Illuminate\Http\Request;
use App\Http\DAO\DAOClient;


class ClientLogic{
    public function IndexLog(){
        $class = new DAOClient();
        return $class->IndexClient();
    }
    public function StoreLog(Request $request){
        $class = new DAOClient();
        return $class->StoreClient($request);
    }
    public function UpdateLog(Request $request,$id_client){
        $class = new DAOClient();
        return $class->UpdateClient($request,$id_client);
    }
    public function DeleteLog($id_client){
        $class = new DAOClient();
        return $class->DeleteClient($id_client);
    }
}
