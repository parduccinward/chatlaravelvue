<?php

namespace App\Http\Bl;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\inmueblem;
use App\Http\DAO\DAOProperty;

class PropertyLogic
{

    public function Storelog(Request $request){
        $class = new DAOProperty();
        return $class->StorageProperty($request);
    }

    public function Searchlog(Request $request){
        $class = new DAOProperty();
        return $class->SearchProperty($request);
    }

    public function Showlog(){
        $class = new DAOProperty();
        return $class->ShowProperty();
    }

    public function Storeimagelog(Request $request){
        $class = new DAOProperty();
        return $class->StorageImage($request);
    }

    public function Showimageslog(){
        $class = new DAOProperty();
        return $class->ShowImages();
    }

    public function UpdatePropertylog(Request $request, $id){
        $class = new DAOProperty();
        return $class->UpdatePropertyDao($request, $id);
    }
    public function DeletePropertylog($id){
        $class = new DAOProperty();
        return $class->DeletePropertyDao($id);
    }
    public function DeleteImagelog($id){
        $class = new DAOProperty();
        return $class->DeleteImageDao($id);
    }

}