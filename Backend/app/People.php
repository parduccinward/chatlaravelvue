<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //
    protected $table = 'people';
    protected $primaryKey = 'person_id';

    protected $fillable = ['first_name',
        'second_name',
        'third_name',
        'last_name',
        'email',
        'phone_number'];

    // Relacion de uno a muchos
    public function clients(){
        return $this->hasMany('App\Client');
    }
    // Relacion de uno a muchos
    public function users(){
        return $this->hasMany('App\User');
    }


}
