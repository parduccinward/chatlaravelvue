<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Notifications\TripReviewWarn;
use App\Notifications\TripReviewFatal;
use App\Notifications\TripReviewError;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*-----------------------------INMUEBLES ROUTES-----------------------------*/
Route::get('/inmuebles','InmueblesController@show');
Route::post('/inmuebles/buscar','InmueblesController@buscador');
Route::get('/inmuebles/showima','InmueblesController@showima');
Route::get('/inmuebles/borrar','InmueblesController@borr');
/*--------------------------------------------------------------------------*/

/*-----------------------------COMPANY ROUTES-------------------------------*/

Route::get('/companys','Companycontroller@show');
Route::get('/companys/{id}','Companycontroller@showid');

/*--------------------------------------------------------------------------*/

/*-----------------------------PEOPLE ROUTES--------------------------------*/
Route::get('/people/index','PeopleController@index')->name('people.index');

Route::get('/people/search/{search}','PeopleController@index')->name('people.search');
/*--------------------------------------------------------------------------*/


/*-----------------------------cLIENT ROUTES--------------------------------*/
Route::get('/client/index','ClientController@index')->name('client.index');

/*--------------------------------------------------------------------------*/


/*-----------------------------CHAT ROUTES--------------------------------*/
Route::get('/chat', 'ChatController@index')->name('chat');
//Route::get('/chat/manageChat', 'ChatController@manageChat')->name('chat.manageChat');
//Route::post('/chat/newMessage', 'ChatController@newMessage')->name('chat.newMessage');
/*--------------------------------------------------------------------------*/


/*-----------------------------REVIEW ROUTES--------------------------------*/

/*--------------------------------------------------------------------------*/



Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
Route::group(['middleware' => ['jwt']], function() {
    Route::post('/ReviewC', 'Trip_ReviewController@create')->name('TripC');
    Route::get('/ReviewS','Trip_ReviewController@show');
    Route::post('/inmuebles/crear','InmueblesController@store');
    Route::post('/inmuebles/images','InmueblesController@imasave');
    Route::put('/inmueblesupdate/{id}','InmueblesController@UpdateProperty');
    Route::delete('/inmueblesdelete/{id}','InmueblesController@DeleteProperty');
    Route::delete('/imagesdelete/{id}','InmueblesController@DeleteImage');
    Route::post('/company','Companycontroller@store');



    Route::put('/companysupdate/{id}','Companycontroller@updateid');
    Route::delete('/companysdelete/{id}','Companycontroller@deleteid');


    Route::post('/people/store','PeopleController@store')->name('people.store');
    Route::post('/people/update/{person_id}','PeopleController@update')->name('people.update');
    Route::get('/people/destroy/{person_id}','PeopleController@destroy')->name('people.destroy');

    Route::post('/client/store','ClientController@store')->name('client.store');
    Route::post('/client/update/{id_client}','ClientController@update')->name('client.update');
    Route::get('/client/destroy/{id_client}','ClientController@destroy')->name('client.destroy');
});
