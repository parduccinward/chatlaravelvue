<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

// Rutas GENERADAS por auth:register,login
Auth::routes();

/**
 *                          BASICO!!
 *      VISTAS : /resources/views/....      Ejm: home.blade.php
 *      CONTROLADORES : /app/Http/Controlollers/....    Ejm: HomeController.php
 *      MODELOS : /app/....       Ejm: User.php
 *      RUTAS : /routes/web.php
 */

// '/' - es la ruta que se vera como pagina inicial
// 'HomeController@index' - HomeController es el CONTROLADOR que utiliza
// 'HomeController@index' - index es el FUNCION dentro del controlador Homecontroller, 
//                          esta funcion es la que manda la 'vista' inicial
//  'home' - para asignar toda esta vista a esa variable
//      ejm 1: 
//           Route::get('/', 'ClienteController@lista')->name('cliente.lista');
//      ejm 2: ->name('NOMBRE_DE_CONTROLADOR.FUNCION_QUE_REALIZA')
Route::get('/', 'HomeController@index')->name('home');
Route::get('/inmuebles/buscar','InmueblesController@buscador');
Route::post('/inmuebles/crear','InmueblesController@store');

Route::get('/inmobiliaria','AgentesController@index')->name('inmobiliaria');
Route::get('/inmobliliaria/registrar','AgentesController@registrar')->name('registrar');
Route::get('/inmobliliaria/modificar','AgentesController@modificar')->name('modificar');
Route::get('/inmobliliaria/eliminar','AgentesController@eliminar')->name('eliminar');

Route::get('/inmobliliaria/registrarcom','AgentesController@registrarcom')->name('registrarcom');
Route::get('/inmobliliaria/modificarcom','AgentesController@modificarcom')->name('modificarcom');
Route::get('/inmobliliaria/eliminarcom','AgentesController@eliminarcom')->name('eliminarcom');
Route::post('/inmobliliaria/crearcom','AgentesController@store');

Route::get('/chat', 'ChatController@index')->name('chat');
Route::get('/message', 'MessageController@index')->name('message');

Route::resource('Trip_Review', 'Trip_ReviewController');
Route::post('/Trip_Review/Crear','Trip_ReviewController@store');
//Route::post('/Trip_review/store', 'Trip_ReviewController@store')->name("Trip_Review.store");
Route::get('/Trip_Review','Trip_ReviewController@list')->name('Trip_Review.list');
Route::get('/Trip_Review/lista','Trip_ReviewController@liste')->name('Trip_Review.liste');
//Route::get('/Trip_Review/Crear','Trip_ReviewController@create')->name('Trip_Review_Crear');
//Route::get('/Trip_Review/Index','Trip_ReviewController@index')->name('Trip_Review_Index');


